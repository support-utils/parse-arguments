# Set minimal cmake requirement
cmake_minimum_required(VERSION 3.15)

# Make use of C14
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Define project
project("project" VERSION 1.0 LANGUAGES CXX)

add_executable(${PROJECT_NAME})

target_include_directories(${PROJECT_NAME} PUBLIC "include")
target_include_directories(${PROJECT_NAME} PRIVATE "src")

file(GLOB SOURCE_FILES "src/*.cpp")
target_sources(${PROJECT_NAME} PUBLIC ${SOURCE_FILES})